from bs4 import BeautifulSoup as bs
import requests
import urllib.request
import os
import sys

platform = sys.platform
user = os.getenv('LOGNAME')

if platform.startswith('linux'):
    save_dir = "/home/{}/Pictures/".format(user)
elif platform.startswith('darwin'):
    save_dir = "/Users/{}/Pictures/apod/".format(user)

page = 'http://apod.nasa.gov/apod/'

def download():
    r = requests.get(page + "astropix.html")
    soup = bs(r.text)
    try:
        new_img = soup('img')[0]['src']
    except IndexError:
        print('No image today')
        return
    new_img_name = new_img.split("/")[-1]
    save_file = save_dir+ new_img_name
    urllib.request.urlretrieve(page + new_img, save_file)
    print('Downloaded {}'.format(new_img_name))
    
if __name__ == "__main__":
    download()
